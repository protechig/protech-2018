<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, ptig_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ProTech 2018
 */

get_header(); 
get_template_part('header-after'); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'ptig_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
