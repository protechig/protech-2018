<?php
/**
 * The template Name: Front page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ProTech 2018
 */

get_header('design'); 
?>

	  <!-- #post-## -->
<div class="blocks">
	  <?php
       if( have_rows('flexible_content')):
	    while(have_rows('flexible_content')): the_row();
		   get_template_part('template-parts/content-blocks/block-' . get_row_layout() );
	    endwhile;
      endif;
       ?>
</div>

<?php get_footer(); ?>
