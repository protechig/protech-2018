<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package ProTech 2018
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'protech' ); ?></h2>
</section>
