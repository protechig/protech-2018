<?php
/**
 * The template used for displaying a Partners block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="partners">
	<div class="wrap">
		<h2 class="brands"><?php the_sub_field('heading_title');?></h2>
		<!--Partner Badge Slider Here-->
		<div class="partners-list">
			<?php
				// check if the repeater field has rows of data
				if (have_rows('logos')):
				// loop through the rows of data
				while (have_rows('logos')): the_row();
			?>

				<div class="logo">
				<!--display a sub field value-->
					<img src="<?php the_sub_field('image_logo')['url'];?>" alt="<?php echo the_sub_field('image_logo')['alt']; ?>" />
				</div>

			<?php
				endwhile;
				else:
				// no rows found
				endif;
			?>
		</div><!-- .partners-list -->
	</div><!-- .wrap -->
</section><!-- .partners -->
