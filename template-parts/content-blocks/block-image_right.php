<?php
/**
 * The template used for displaying an Image Right block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="other-services-offered" style="background: <?php the_sub_field('background_color');?>">
	<div class="wrap">
		<div class="service-wrap">
			<div class="services-right">
				<?php if (have_rows('right_items')): ?>
				<?php while (have_rows('right_items')): the_row();?>

					<div class="right-section">
						<div class="flex-item service text-center">
							<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />

							<h3><?php the_sub_field('title');?></h3>

							<p><?php the_sub_field('title_content');?> </p>
						</div><!-- .service -->
					</div><!-- .right-section -->

				<?php endwhile;?>
				<?php endif;?>
			</div><!-- .service-right -->

			<div class="right-image">
				<img class="img-position" src="<?php the_sub_field('right_image')['url'];?>" alt="<?php echo the_field('right_image')['alt']; ?>" />
			</div><!-- .image-right -->
		</div><!-- .service-wrap -->
	</div><!-- .wrap -->
</section><!-- .other-services-offered -->