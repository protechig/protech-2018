<?php
/**
 * The template used for displaying a Left Image block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<div class="services">
    <div class="wrap">
        <?php 
            if( have_rows('right_image') ):
            while( have_rows('right_image') ): the_row(); 
        ?>

        <div class="service-content">
            <h3><?php the_sub_field('title'); ?></h3>
            
            <p><?php the_sub_field('service_content'); ?></p>
        </div><!-- .service-content -->

        <div class="right-image">
            <img src="<?php echo the_sub_field('right_image'); ?>">
        </div><!-- .right-image -->

        <?php 
            endwhile;
            endif;
        ?>
    </div><!-- .wrap -->
</div><!-- .service -->
