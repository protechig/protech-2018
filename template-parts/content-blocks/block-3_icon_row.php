<?php
/**
 * The template used for displaying a 3 Icon row.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="our-services" style="background: <?php the_sub_field('background_color');?>">
	<div class="wrap">
		<h2 class="service-title" style="text-align: center; color: <?php the_sub_field('header_color');?>"><?php the_sub_field('header_title');?></h2>

		<p class="service-intro"><?php the_sub_field('header_description');?></p>

	    <div class="services-offered">
			<?php
				$repeater = get_sub_field('services_list');
				foreach($repeater as $row) {
			?>

				<div class="flex-item service">
					<img src="<?php echo $row['image'];?>" />

						<h3><?php echo $row['service_title'];?></h3>

						<p><?php echo $row['service_info'];?> </p>
				</div><!-- .service -->

			<?php 
				} 
			?>
	    </div><!-- .services-offered -->
	</div><!-- .wrap -->
</section><!-- .our-services -->