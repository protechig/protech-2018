<?php
/**
 * The template used for displaying a Left Heading block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<div class="row block-image_right d-flex flex-row">
    <div class="col-12 col-md-6 order-2 order-md-1">
        <h1><?php the_sub_field('ltitle'); ?></h1>
        <p><?php  the_sub_field('lcontent'); ?></p>
    </div>

    <div class="col-12 col-md-6 d-flex align-items-center justify-content-center order-1 order-md-2">
        <img src="<?php the_sub_field('limage'); ?>">
    </div>
</div><!-- .wrap -->