<?php
/**
 * The template used for displaying an Intro block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="intro" style="background: url('<?php the_sub_field('background_image'); ?>') bottom center no-repeat;">
	<div class="wrap">
		<div class="content">
			<h1 class="title"><?php the_sub_field('title');?></h1>

			<p class="tagline"><?php the_sub_field('tagline');?></p>
			
			<a class="button button-intro" href="<?php the_sub_field('button_link');?>"><?php the_sub_field('button');?></a>
		</div><!-- .content -->
	</div><!-- .wrap -->
</section><!-- .intro -->