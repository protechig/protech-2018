<?php
/**
 * The template used for displaying a Featured Project block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<?php
$postid = get_sub_field('portfolio');
?>

<div class="featured-project" style="">
	<article  class="background-color" style="background-image: url('<?php the_field('background_image', $postid)?>'); background-repeat: no-repeat; background-position: center; background-size: 40rem;" id="post-<?php the_ID();?>" <?php post_class();?>>
		<div class="entry-content" >
			<div class="site-description " style="color: <?php the_field('content_color', $postid)?>;">
				<h4 class="sit-name"><?php echo the_field('site_name', $postid) ?></h4>
				
				<h3 class="entry-title"><a style="color:<?php the_field('content_color', $postid)?>;"  href="<?php get_the_permalink($postid);?>"><?php echo get_the_title($postid); ?></a></h3>

				<p class="f-content"> <?php echo the_field('excerpt', $postid); ?></p>

				<a href="<?php echo the_field('button_link', $postid); ?>" rel="bookmark" class="button round button-trinary"><?php the_field('button', $postid); ?></a>			
			</div><!-- entry-content -->

			<div class="screenshot">
				<div class="wrap">
					<img src="<?php the_field('site_image', $postid);?>">
				</div><!-- wrap -->
			</div><!-- screenshot -->
		</div><!-- .entry-content -->
	</article><!-- #post-## -->
</div><!-- featured-project -->