<?php
/**
 * The template used for displaying an Image Left block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="other-services-offered" style="background: <?php the_sub_field('background_color');?>">
	<div class="wrap">
		<h2 class="service-title" style="text-align: center; color: <?php the_sub_field('header_color');?>"><?php the_sub_field('title');?></h2>

		<p class="service-intro"><?php the_sub_field('intro_content');?></p>

		<div class="service-wrap">
			<div class="left-image">
				<img class="img-position" src="<?php the_sub_field('left_image')['url'];?>" alt="<?php echo the_field('left_image')['alt']; ?>" />
			</div><!-- .left-image -->

			<div class="services-left">
				<?php if (have_rows('left_items')): ?>
				<?php while (have_rows('left_items')): the_row();?>

					<div class="right-section">
						<div class="flex-item service text-center">
							<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />

							<h3><?php the_sub_field('title');?></h3>

							<p><?php the_sub_field('title_intro');?> </p>
						</div><!-- .service -->
					</div><!-- .right-section -->

				<?php endwhile;?>
				<?php endif;?>
			</div><!-- .services-left -->
		</div><!-- .service-wrap -->
	</div><!-- .wrap -->
</section><!-- .other-services-offered -->

