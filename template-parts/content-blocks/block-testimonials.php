<?php
/**
 * The template used for displaying a Testimonials block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="testimonials" style="">
	<div class="wrap">
		<div class="contributors">
			<div class="opinion">
				<img src="<?php the_sub_field('quote_image')['url'];?>" alt="<?php echo the_sub_field('quote-image')['alt']; ?>" />
				<p><?php the_sub_field('content');?></p>
			</div><!-- .opinion -->

			<div class="client">
				<div class="client-img">
					<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />
				</div><!-- .client-img -->
				
				<div class="details">
					<p class="name"><?php the_sub_field('name');?></p>
					<p class="title"><?php the_sub_field('position');?></p>
				</div><!-- .details -->
			</div><!-- .client -->
		</div><!-- .contributers -->
	</div><!-- .wrap -->
</section><!-- .testimonials -->