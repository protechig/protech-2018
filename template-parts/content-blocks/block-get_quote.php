<?php
/**
 * The template used for displaying a Quote block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="quote" style="background-color:<?php the_sub_field('background_col-block');?>;">
	<div class="wrap">
		<div class="quote-area">
			<!--display a sub field value-->
			<p style="color:<?php the_sub_field('paragraph_color');?>"><?php the_sub_field('for_information');?></p>
			
			<a href="<?php the_sub_field('button_link') ?>" class="button-secondary button round"><?php the_sub_field('button');?></a>
		</div><!-- .quote-area -->
	</div><!-- .wrap -->
</section><!-- .quote -->
