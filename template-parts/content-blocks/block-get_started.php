<?php
/**
 * The template used for displaying a Get Started block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="get-started" style="background-color:<?php the_sub_field('background_color');?>">
	<div class="wrap">
		<p style="color:<?php the_sub_field('text_color');?>"><?php the_sub_field('content_text');?></p>

		<a href="<?php the_sub_field('button_link') ?>" class="button button-trinary round"><?php the_sub_field('button');?></a>
	</div><!-- .wrap -->
</section><!-- .get-started -->