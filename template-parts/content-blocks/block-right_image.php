<?php
/**
 * The template used for displaying a Right Image block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="services">
    <div class="wrap">
        <?php 
            if( have_rows('right_image') ):
            while( have_rows('right_image') ): the_row();
        ?>

        <div class="services-content">
            <h3><?php the_sub_field('title'); ?></h3>

            <p><?php the_sub_field('service_content'); ?></p>
        </div><!-- .services-content -->

        <div class="left-image">
            <img src="<?php echo the_sub_field('left_image'); ?>">
        </div><!-- .image-left -->

        <?php 
            endwhile;
            endif; 
        ?>
    </div><!-- .wrap -->
</section><!-- .services -->
