<?php
/**
 * The template used for displaying a Blog block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<div class="featured-blog">
	<div class="wrap">
		<h2>From The Blog</h2>
	</div><!-- .wrap -->

	<div class="wrap">

	<?php

		// WP_Query arguments
		$args = array(

			'posts_per_page' => '3',
			'order' => 'DESC',
			'orderby' => 'id',
		);

		// The Query
		$blog = new WP_Query($args);

		// The Loop
		if ($blog->have_posts()) {
			while ($blog->have_posts()) {
				$blog->the_post();
				// do something
	?>

		<div class="blog-post">
			<?php the_title('<h3 class="entry-header"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>');?>

			<div class="entry-meta">
				<?php echo get_the_category()[0]->name; ?>
			</div><!-- .entry-meta -->

			<div class="entry-content">
					<?php 
					echo '<a href="' . get_permalink() . '">' . get_the_excerpt() . ' . . . </a>';

					// the_excerpt('<a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a>'); 
					
					?>
			</div> <!-- entry-content -->
		</div> <!-- blog-post -->
	
	<?php
			}
		} 

		else {
			// no posts found
		}

		// Restore original Post Data
		wp_reset_postdata();
		
		?>

	</div> <!-- wrap -->
</div> <!-- featured-blog -->
