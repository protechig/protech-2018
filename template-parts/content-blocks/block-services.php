<?php
/**
 * The template used for displaying a Services block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>


<section class="services" style="background-color:<?php the_sub_field('background_color');?>">
	<div class="wrap wide">
		<h2 class="services-title"><?php the_sub_field('header');?></h2>

		<p class="services-intro"><?php the_sub_field('content');?></p>

		<div class="services-list">
			<?php 
				if (have_rows('services')): 
				while (have_rows('services')): the_row();
			?>
				<div class="flex-item service text-center">
					<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />

					<h3><?php the_sub_field('title');?></h3>

					<p><?php the_sub_field('content');?></p>

					<a class="button small round button-trinary" href="<?php the_sub_field('button_link');?>"><?php the_sub_field('button');?></a>
				</div><!-- .service -->
				
				<hr>
			<?php 
				endwhile;
				endif;
			?>
		</div><!-- .services-list -->
	</div><!-- .wrap -->
</section><!-- .services -->