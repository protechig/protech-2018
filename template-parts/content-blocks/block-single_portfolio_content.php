<?php
/**
 * The template used for displaying a Single Portfolio block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="single-portfolio-content">
	<div class="wrap wrap-extra-wide">
		<div class="image">
			<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />
		</div><!-- .image -->
		
		<div class="contents">
			<h3><?php the_sub_field('title');?></h3>

			<p><?php the_sub_field('content');?></p>

			<a href="<?php the_sub_field('button_link') ?>" class="button button-secondary round"><?php the_sub_field('button');?></a>
		</div><!-- .contents -->
	</div><!-- .wrap -->
</section><!-- .single-portfolio-content -->