
<?php
/**
 * The template used for displaying a 3 Column Icon block.
 *
 * @package ProTech 2018
 */

// Set up fields.
?>

<section class="other-services-offered" style="background: <?php the_sub_field('bg_color');?>">
	<div class="wrap">
		<h2 class="service-title" style="text-align: center; color: <?php the_sub_field('header_color');?>"><?php the_sub_field('heading');?></h2>

		<p class="service-intro"><?php the_sub_field('description');?></p>

		<div class="service-wrap">
			<div class="services-offered">
				<?php if (have_rows('left_service_offered')): ?>
				<?php while (have_rows('left_service_offered')): the_row();?>

					<div class="left-section">
						<div class="flex-item service">
							<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />

							<h3><?php the_sub_field('title');?></h3>

							<p><?php the_sub_field('service_description');?></p>
						</div><!-- .service -->
					</div><!-- .left-section -->

				<?php endwhile;?>
				<?php endif;?>
			</div><!-- .services-offered -->

			<div class="centered-image">
				<img src="<?php the_sub_field('center_image')['url'];?>" alt="<?php echo the_field('center_image')['alt']; ?>" />
			</div><!-- .centered-image -->

			<div class="services-offered">
				<?php if (have_rows('right_service_offered')): ?>
				<?php while (have_rows('right_service_offered')): the_row();?>

					<div class="right-section">
						<div class="flex-item service">
							<img src="<?php the_sub_field('image')['url'];?>" alt="<?php echo the_sub_field('image')['alt']; ?>" />

							<h3><?php the_sub_field('title');?></h3>

							<p><?php the_sub_field('service_description');?></p>
						</div><!-- .service -->
					</div><!-- .right-section -->
					
				<?php endwhile;?>
				<?php endif;?>
			</div><!-- .services-offered -->
		</div><!-- .service-wrap -->
	</div><!-- .wrap -->
</section><!-- .other-services-offered -->

