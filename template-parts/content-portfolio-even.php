<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ProTech 2018
 */

?>

<article  class="background-color" id="post-<?php the_ID();?>" <?php post_class();?> style="background: <?php the_field('background_col');?>">
	<div class="entry-content even" style="background-image: url('<?php the_field('background_img')?>;">
	  <div class="screenshot">
			<div class="wrap">
				<a href="<?php the_field('button_link'); ?>" rel="bookmark"><img src="<?php the_field('site_image');?>"></a>
			</div>
	  </div><!-- screenshot -->

		<div class="site-description spaceleft" style="color: <?php the_field('content_color')?>;">
			<h4 class="sit-name"><?php the_field('site_name'); ?></h4>

			<?php the_title('<h3 class="entry-title even-title"><a style="color:' .  get_field('content_color') .  ';"  href="' . get_field('button_link') . '" rel="bookmark">', '</a></h3>');?>

			<p><?php the_field('excerpt');?></p>

			<a href="<?php the_field('button_link'); ?>" rel="bookmark" class="button-trinary button round"><?php the_field('button'); ?></a>
		</div><!-- site-description spaceleft -->
	</div><!-- .entry-content -->
</article><!-- #post-## -->
