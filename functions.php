<?php
/**
 * ProTech 2018 functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package ProTech 2018
 */

if (!function_exists('ptig_setup')):
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */

    function ptig_setup()
{
        /**
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on ProTech 2018, use a find and replace
         * to change 'protech' to the name of your theme in all the template files.
         * You will also need to update the Gulpfile with the new text domain
         * and matching destination POT file.
         */
        load_theme_textdomain('protech', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /**
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /**
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');
        add_image_size('full-width', 1920, 1080, false);
		add_image_size('blog_grid', 660, 460, true);
		

        // Register navigation menus.
        register_nav_menus(array(
            'primary' => esc_html__('Primary Menu', 'protech'),
            'mobile' => esc_html__('Mobile Menu', 'protech'),
        ));

        /**
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('ptig_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Custom logo support.
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 500,
            'flex-height' => true,
            'flex-width' => true,
            'header-text' => array('site-title', 'site-description'),
        ));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif; // ptig_setup
add_action('after_setup_theme', 'ptig_setup');
/**
 * Excerpt limit
 */
function custom_excerpt_length($length)
{
    return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ptig_content_width()
{
    $GLOBALS['content_width'] = apply_filters('ptig_content_width', 640);
}
add_action('after_setup_theme', 'ptig_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function ptig_widgets_init()
{

    // Define sidebars.
    $sidebars = array(
        'sidebar-1' => esc_html__('Sidebar 1', 'protech'),
        // 'sidebar-2'  => esc_html__( 'Sidebar 2', 'protech' ),
        // 'sidebar-3'  => esc_html__( 'Sidebar 3', 'protech' ),
    );

    // Loop through each sidebar and register.
    foreach ($sidebars as $sidebar_id => $sidebar_name) {
        register_sidebar(array(
            'name' => $sidebar_name,
            'id' => $sidebar_id,
            'description' => /* translators: the sidebar name */sprintf(esc_html__('Widget area for %s', 'protech'), $sidebar_name),
            'before_widget' => '<aside class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h2 class="widget-title">',
            'after_title' => '</h2>',
        ));
    }

}
add_action('widgets_init', 'ptig_widgets_init');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';

if (!function_exists('Portfolio_post')) {

    // Register Custom Post Type
    function portfolio_post()
    {

        $labels = array(
            'name' => _x('Portfolio', 'Post Type General Name', 'Portfolio'),
            'singular_name' => _x('Portfolio', 'Post Type Singular Name', 'Portfolio'),
            'menu_name' => __('Portfolio', 'Portfolio'),
            'name_admin_bar' => __('Portfolio Post', 'Portfolio'),
            'archives' => __('Portfolio Archives', 'Portfolio'),
            'attributes' => __('Portfolio Attributes', 'Portfolio'),
            'parent_item_colon' => __('Parent Portfolio:', 'Portfolio'),
            'all_items' => __('All Portfolio', 'Portfolio'),
            'add_new_item' => __('Add New Portfolio', 'Portfolio'),
            'add_new' => __('Add Portfolio', 'Portfolio'),
            'new_item' => __('New Portfolio', 'Portfolio'),
            'edit_item' => __('Edit Portfolio', 'Portfolio'),
            'update_item' => __('Update Portfolio', 'Portfolio'),
            'view_item' => __('View Portfolio', 'Portfolio'),
            'view_items' => __('View Portfolio', 'Portfolio'),
            'search_items' => __('Search Portfolio', 'Portfolio'),
            'not_found' => __('Not found', 'Portfolio'),
            'not_found_in_trash' => __('Not found in Trash', 'Portfolio'),
            'featured_image' => __('Portfolio Featured Image', 'Portfolio'),
            'set_featured_image' => __('Set Portfolio featured image', 'Portfolio'),
            'remove_featured_image' => __('Remove Portfolio featured image', 'Portfolio'),
            'use_featured_image' => __('Use as Portfolio featured image', 'Portfolio'),
            'insert_into_item' => __('Insert into Portfolio', 'Portfolio'),
            'uploaded_to_this_item' => __('Uploaded to this Portfolio', 'Portfolio'),
            'items_list' => __('Portfolio list', 'Portfolio'),
            'items_list_navigation' => __('Portfolio list navigation', 'Portfolio'),
            'filter_items_list' => __('Filter Portfolio list', 'Portfolio'),
        );
        $args = array(
            'label' => __('Portfolio', 'Portfolio'),
            'description' => __('Post Type Description', 'Portfolio'),
            'labels' => $labels,
            'supports' => array('title', 'editor', 'thumbnail'),
            'taxonomies' => array('category'),
            'hierarchical' => false,
            'public' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'menu_position' => 10,
            'menu_icon' => 'dashicons-chart-line',
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
            'publicly_queryable' => true,
            'capability_type' => 'page',
        );
        register_post_type('portfolio', $args);

    }
    add_action('init', 'portfolio_post', 0);

}
//custom fields option page
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false,
    ));
}

//google map ACF hooks

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyAd5OH_C-7JMxC6OuAPsQZ4TwMcJ0zctY8');
}

add_action('acf/init', 'my_acf_init');

// Modify read more link text

function new_excerpt_more($more) {
    global $post;
 return '';
}
add_filter('excerpt_more', 'new_excerpt_more');